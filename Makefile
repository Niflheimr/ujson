VPATH=src

PREFIX?=/usr/local
LIBDIR?=$(PREFIX)/lib
INCDIR?=$(PREFIX)/include

CFLAGS+=-std=c99 -Wall -Wextra -fPIC
LDLIBS+=-lm

DESTDIR?=

ifeq ($(DEBUG),1)
CFLAGS+=-Og -ggdb
endif

all: libujson.so

libujson.so: ujson.o
	$(CC) -shared $(LDFLAGS) -o $@ $^ $(LDLIBS)

install: all
	mkdir -p "$(DESTDIR)$(LIBDIR)"
	mkdir -p "$(DESTDIR)$(INCDIR)"
	cp -fp libujson.so "$(DESTDIR)$(LIBDIR)"
	cp -fp "$(VPATH)/ujson.h" "$(DESTDIR)$(INCDIR)"

uninstall:
	rm -f "$(DESTDIR)$(LIBDIR)/libujson.so"
	rm -f "$(DESTDIR)$(INCDIR)/ujson.h"

clean:
	rm -f libujson.so
	rm -f ujson.o

.PHONY: all install uninstall clean
