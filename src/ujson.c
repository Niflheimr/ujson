/* Copyright (C) 2018 Sam Bazley and Arnau Bigas
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "ujson.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define INITIAL_SIZE 4

#define E_VALUE (1<<0)
#define E_COMMA (1<<1)
#define E_KEY   (1<<2)
#define E_COLON (1<<3)
#define E_CLOSE (1<<4)

#define ERR_FMAT "%s:%u:%u: "
#define ERR_ARGS info.path, info.line, info.column

#define JSON_ERROR(err, format, ...) \
{ \
    if (err != 0) { \
        err->file = __FILE__; \
        err->line = __LINE__; \
        err->func = __func__; \
        size_t s = snprintf(NULL, 0, format, ##__VA_ARGS__) + 1; \
        err->msg = malloc(s); \
        sprintf(err->msg, format, ##__VA_ARGS__); \
    } \
}

typedef struct FileInfo {
    const char *path;
    int line;
    int column;
} FileInfo;

static char *typeStrings [] = {
    "JSON_STRING",
    "JSON_NUMBER",
    "JSON_OBJECT",
    "JSON_ARRAY",
    "JSON_TRUE",
    "JSON_FALSE",
    "JSON_NULL"
};

void jsonErrorInit(JsonError *err)
{
    err->line = 0;
}

void jsonErrorCleanup(JsonError *err)
{
    if (jsonErrorIsSet(err)) {
        free(err->msg);
    }
}

int jsonErrorIsSet(JsonError *err)
{
    return err->line > 0;
}

static int foundString(char *file, int index, char **str,
        FileInfo info, JsonError *err)
{
    index++;

    int length = 0;
    int delta = 0;
    int canceled = 0;

    for (int i = 0; 1; i++) {
        char c = file[index+i];

        if (c == '\0') {
            JSON_ERROR(err,
                    ERR_FMAT"Reached end of file before end of string",
                    ERR_ARGS);
            return 0;
        } else if (c == '\n' || c == '\r') {
            JSON_ERROR(err,
                    ERR_FMAT"Reached end of line before end of string",
                    ERR_ARGS);
            return 0;
        }

        if (canceled) {
            switch (c) {
            case '"':
            case '\\':
            case '/':
            case 'b':
            case 'f':
            case 'n':
            case 'r':
            case 't':
                length--; // '\\' 'x' -> '\x'
                break;
            case 'u':
                length -= 4; // '\\' 'u' '0' '0' '0' '0' -> 'xx'
                break;
            default:
                JSON_ERROR(err,
                        ERR_FMAT"Invalid canceled character in string",
                        ERR_ARGS);
                return 0;
            }
            canceled = 0;
        } else if (c == '\\') {
            canceled = 1;
        } else if (c == '"') {
            break;
        }

        length++;
        delta++;
    }

    *str = malloc(length + 1);

    int i = 0;
    for (int ci = 0; ci < delta; ci++) {
        char c = file[index+ci];

        if (canceled) {
            switch (c) {
            case '"':
            case '\\':
            case '/':
                (*str)[i++] = c;
                break;
            case 'b':
                (*str)[i++] = '\b';
                break;
            case 'f':
                (*str)[i++] = '\f';
                break;
            case 'n':
                (*str)[i++] = '\n';
                break;
            case 'r':
                (*str)[i++] = '\r';
                break;
            case 't':
                (*str)[i++] = '\t';
                break;
            case 'u':
                {
                if (delta - ci < 4) {
                    JSON_ERROR(err,
                            ERR_FMAT"Invalid unicode character in string",
                            ERR_ARGS);
                    return 0;
                }

                char *cs = file+index+ci+1;
                char c0 [3] = {cs[0], cs[1], 0}, c1 [3] = {cs[2], cs[3], 0};

                int u0 = strtol(c0, 0, 16);
                int u1 = strtol(c1, 0, 16);

                if (u0)
                    (*str)[i++] = u0;
                if (u1)
                    (*str)[i++] = u1;

                ci += 4;
                }
            }
            canceled = 0;
        } else if (c == '\\') {
            canceled = 1;
        } else {
            (*str)[i++] = c;
        }
    }

    (*str)[length] = 0;

    return delta + 1; //+1 to remove '"'
}

static int foundNumber(char *file, int index, double *nmbr,
        FileInfo info, JsonError *err)
{
    int negative = (file[index] == '-');
    int length = negative;
    char c = file[index+length];
    double output = 0;

    if (!(c >= '0' && c <= '9')) {
        JSON_ERROR(err,
                ERR_FMAT"Unexpected character in number", ERR_ARGS);
        return 0;
    }

    while (c >= '0' && c <= '9') {
        output *= 10;
        output += c - '0';

        length++;
        c = file[index+length];
    }

    if (c == '.') {
        length++;
        c = file[index+length];

        if (!(c >= '0' && c <= '9')) {
            JSON_ERROR(err,
                    ERR_FMAT"Unexpected character in number",
                    ERR_ARGS);
            return 0;
        }

        int decimals = 0;
        while (c >= '0' && c <= '9') {
            output *= 10;
            output += c - '0';

            decimals++;
            length++;
            c = file[index+length];
        }
        output /= pow(10, decimals);
    }

    if (c == 'e' || c == 'E') {
        length++;
        c = file[index+length];
        int negExp = 0;

        if (c == '+') {
            length++;
        } else if (c == '-') {
            length++;
            negExp = 1;
        }

        c = file[index+length];

        if (!(c >= '0' && c <= '9')) {
            JSON_ERROR(err,
                    ERR_FMAT"Unexpected character in number",
                    ERR_ARGS);
            return 0;
        }

        double exp = 0;
        while (c >= '0' && c <= '9') {
            exp *= 10;
            exp += c - '0';

            length++;
            c = file[index+length];
        }

        output = output * pow(10, exp*(negExp?-1:1));
    }

    *nmbr = output * (negative?-1:1);
    return length;
}

static void _addValue(char *key, void *val, void *parent, JsonError *err)
{
    JsonType parentType = jsonGetType(parent);

    (void) err;

    if (parentType == JSON_OBJECT) {
        JsonPair pair;
        pair.key = key;
        pair.val = val;
        pair.object = parent;

        JsonObject *cObj = parent;
        if (cObj->size == 0) {
            cObj->size = INITIAL_SIZE;
            cObj->pairs = malloc(cObj->size * sizeof(JsonPair));
        } else if (cObj->used == cObj->size) {
            cObj->size *= 2;
            cObj->pairs= realloc(cObj->pairs, cObj->size * sizeof(JsonPair));
        }

        cObj->pairs[cObj->used++] = pair;
    } else if (parentType == JSON_ARRAY) {
        JsonArray *cArr = parent;
        if (cArr->size == 0) {
            cArr->size = INITIAL_SIZE;
            cArr->vals = malloc(cArr->size * sizeof(void *));
        } else if (cArr->used == cArr->size) {
            cArr->size *= 2;
            cArr->vals = realloc(cArr->vals, cArr->size * sizeof(void *));
        }

        cArr->vals[cArr->used++] = val;
    }
}

#define MALLOCKEY(ret) \
{ \
    if (parent == 0) { \
        JSON_ERROR(err, "Cannot add value to null parent"); \
        return ret; \
    } \
    JsonType pType = jsonGetType(parent); \
    char *_key = 0; \
    if (pType == JSON_OBJECT) { \
        if (key) { \
            _key = malloc(strlen(key) + 1); \
            strcpy(_key, key); \
        } else { \
            JSON_ERROR(err, "Cannot add value to %s with null key", \
                    typeStrings[pType]); \
            return ret; \
        } \
    } else if (pType != JSON_ARRAY) { \
        JSON_ERROR(err, "Cannon add value to %s", typeStrings[pType]); \
        return ret; \
    } \
    key = _key; \
}

void jsonAddString(char *key, char *str, void *parent, JsonError *err)
{
    if (str == 0) {
        JSON_ERROR(err, "Cannot add null string");
        return;
    }

    MALLOCKEY()
    char *_str = malloc(strlen(str) + 1);
    strcpy(_str, str);

    JsonString *val = malloc(sizeof(JsonString));
    val->type = JSON_STRING;
    val->parent = parent;
    val->data = _str;
    _addValue(key, val, parent, err);
}

void jsonAddNumber(char *key, double number, void *parent, JsonError *err)
{
    MALLOCKEY()

    JsonNumber *val = malloc(sizeof(JsonNumber));
    val->type = JSON_NUMBER;
    val->parent = parent;
    val->data = number;
    _addValue(key, val, parent, err);
}

JsonObject *jsonAddObject(char *key, void *parent, JsonError *err)
{
    MALLOCKEY(0)

    JsonObject *val = malloc(sizeof(JsonObject));
    val->type = JSON_OBJECT;
    val->parent = parent;
    val->size = val->used = 0;
    _addValue(key, val, parent, err);

    return val;
}

JsonArray *jsonAddArray(char *key, void *parent, JsonError *err)
{
    MALLOCKEY(0)

    JsonArray *val = malloc(sizeof(JsonArray));
    val->type = JSON_ARRAY;
    val->parent = parent;
    val->size = val->used = 0;
    _addValue(key, val, parent, err);

    return val;
}

void jsonAddBoolNull(char *key, JsonType type, void *parent, JsonError *err)
{
    if (type != JSON_TRUE && type != JSON_FALSE && type != JSON_NULL) {
        JSON_ERROR(err, "Error creating bool/null JSON entry");
        return;
    }

    MALLOCKEY()

    JsonTrueFalseNull *val = malloc(sizeof(JsonTrueFalseNull));
    val->type = type;
    val->parent = parent;
    _addValue(key, val, parent, err);
}

static void parentStructure(void **currentValue, FileInfo info, JsonError *err)
{
    JsonType currentType = jsonGetType(*currentValue);
    if (currentType == JSON_OBJECT) {
        JsonObject *obj = *currentValue;
        *currentValue = obj->parent;
    } else if (currentType == JSON_ARRAY) {
        JsonArray *arr = *currentValue;
        *currentValue = arr->parent;
    } else {
        JSON_ERROR(err,
                ERR_FMAT"Current JSON value not Object/Array",
                ERR_ARGS);
    }
}

JsonObject *jsonCreateBaseObject()
{
    JsonObject *base = malloc(sizeof(JsonObject));
    base->type = JSON_OBJECT;
    base->parent = 0;
    base->used = 0;
    base->size = 0;
    return base;
}

static JsonObject *parse(char *data, const char *path, JsonError *err)
{
    char *key = 0;
    int depth = 0;

    FileInfo info;
    info.path = path;
    info.line = 1;
    info.column = 0;

    int expecting = E_VALUE;

    JsonObject *base = 0;
    void *currentValue = 0;

    if (data == 0) {
        JSON_ERROR(err, "Data is NULL");
        return 0;
    }

    if (strlen(data) == 0) {
        JSON_ERROR(err, "Empty string");
        return 0;
    }

    for (size_t i = 0; i < strlen(data); i++) {
        char c = data[i];

        if (c == '\n') {
            info.line++;
            info.column = 0;
            continue;
        } else if (c == '\r') {
            continue;
        }

        info.column++;

        if (c == ' ' || c == '\t') {
            continue;
        }

        if (depth == 0 && c != '{') {
            JSON_ERROR(err,
                    ERR_FMAT"Unexpected character %c, expecting '{'",
                    ERR_ARGS, c);
            return 0;
        }

        if (expecting & E_VALUE && c == '{') {
            if (depth == 0) {
                base = currentValue = jsonCreateBaseObject();
            } else {
                currentValue = jsonAddObject(key, currentValue, err);
                if (key) {
                    free(key);
                    key = 0;
                }
            }
            depth++;
            expecting = E_KEY | E_CLOSE;
        } else if (expecting & E_VALUE && c == '[') {
            currentValue = jsonAddArray(key, currentValue, err);
            if (key) {
                free(key);
                key = 0;
            }
            depth++;
            expecting = E_VALUE | E_CLOSE;
        } else if (expecting & E_VALUE && c == '"') {
            char *str;
            int delta = foundString(data, i, &str, info, err);
            if (!delta) {
                goto end;
            }
            i += delta;
            info.column += delta;
            jsonAddString(key, str, currentValue, err);
            if (key) {
                free(key);
                key = 0;
            }
            free(str);

            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_VALUE &&
            (c == '-' || (c >= '0' && c <= '9'))) {
            double nmbr;
            int delta = foundNumber(data, i, &nmbr, info, err);
            if (!delta) {
                goto end;
            }
            i += delta - 1;
            info.column += delta - 1;
            jsonAddNumber(key, nmbr, currentValue, err);

            if (key) {
                free(key);
                key = 0;
            }

            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_VALUE && sizeof(data)-i-4 > 0 &&
                                          data[i+0] == 't' &&
                                          data[i+1] == 'r' &&
                                          data[i+2] == 'u' &&
                                          data[i+3] == 'e') {
            jsonAddBoolNull(key, JSON_TRUE, currentValue, err);
            if (key) {
                free(key);
                key = 0;
            }
            i += 3;
            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_VALUE && sizeof(data)-i-5 > 0 &&
                                          data[i+0] == 'f' &&
                                          data[i+1] == 'a' &&
                                          data[i+2] == 'l' &&
                                          data[i+3] == 's' &&
                                          data[i+4] == 'e') {
            jsonAddBoolNull(key, JSON_FALSE, currentValue, err);
            if (key) {
                free(key);
                key = 0;
            }
            i += 4;
            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_VALUE && sizeof(data)-i-4 > 0 &&
                                          data[i+0] == 'n' &&
                                          data[i+1] == 'u' &&
                                          data[i+2] == 'l' &&
                                          data[i+3] == 'l') {
            jsonAddBoolNull(key, JSON_NULL, currentValue, err);
            i += 3;
            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_COMMA && c == ',') {
            if (jsonGetType(currentValue) == JSON_OBJECT) {
                expecting = E_KEY;
            } else {
                expecting = E_VALUE;
            }
        } else if (expecting & E_CLOSE && c == '}' &&
                jsonGetType(currentValue) == JSON_OBJECT) {
            if (depth > 0)
                parentStructure(&currentValue, info, err);
            depth--;
            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_CLOSE && c == ']' &&
                jsonGetType(currentValue) == JSON_ARRAY) {
            parentStructure(&currentValue, info, err);
            depth--;
            expecting = E_CLOSE | E_COMMA;
        } else if (expecting & E_KEY && c == '"') {
            int delta = foundString(data, i, &key, info, err);
            if (!delta) {
                goto end;
            }
            i += delta;
            info.column += delta;
            expecting = E_COLON;
        } else if (expecting & E_COLON && c == ':') {
            expecting = E_VALUE;
        } else {
            char *first, *second = 0;
            for (int i = 0; i < 2; i++) {
                char **set;
                if (i == 0)
                    set = &first;
                else
                    set = &second;

                if (expecting & E_VALUE) {
                    char str [] = "value";
                    *set = malloc(strlen(str) + 1);
                    strcpy(*set, str);
                    expecting &= ~E_VALUE;
                } else if (expecting & E_COMMA) {
                    char str [] = "','";
                    *set = malloc(strlen(str) + 1);
                    strcpy(*set, str);
                    expecting &= ~E_COMMA;
                } else if (expecting & E_KEY) {
                    char str [] = "key";
                    *set = malloc(strlen(str) + 1);
                    strcpy(*set, str);
                    expecting &= ~E_KEY;
                } else if (expecting & E_COLON) {
                    char str [] = "':'";
                    *set = malloc(strlen(str) + 1);
                    strcpy(*set, str);
                    expecting &= ~E_COLON;
                } else if (expecting & E_CLOSE) {
                    char str [] = "' '";
                    if (jsonGetType(currentValue) == JSON_OBJECT) {
                        str[1] = '}';
                    } else {
                        str[1] = ']';
                    }
                    *set = malloc(strlen(str) + 1);
                    strcpy(*set, str);
                    expecting &= ~E_CLOSE;
                }
            }

            JSON_ERROR(err,
                    ERR_FMAT"Unexpected character '%c', expecting %s%s%s",
                    ERR_ARGS,
                    c, first, (second ? " or " : ""), (second ? second : ""));

            free(first);
            if (second)
                free(second);

            goto end;
        }

        if (depth == 0)
            break;
    }

    if (depth != 0) {
        JSON_ERROR(err,
                ERR_FMAT"Reached end of file, incomplete JSON", ERR_ARGS);
    }

end:
    return base;
}

JsonObject *jsonParseFile(const char *path, JsonError *err)
{
    FILE *file = fopen(path, "r");
    if (!file) {
        JSON_ERROR(err, "Failed to open JSON file");
        return 0;
    }

    fseek(file, 0, SEEK_END);

    size_t fSize = ftell(file) + 1;
    char *data = malloc(fSize);

    data[fSize - 1] = 0;

    rewind(file);
    fread(data, 1, fSize, file);
    fclose(file);

    JsonObject *jo = parse(data, path, err);

    free(data);

    return jo;
}

JsonObject *jsonParseString(char *data, JsonError *err)
{
    return parse(data, "", err);
}

JsonType jsonGetType(void *v)
{
    return ((JsonType *) v)[0];
}

int jsonGetPairIndex(JsonObject *obj, const char *key)
{
    for (unsigned int i = 0; i < obj->used; i++) {
        if (strcmp(obj->pairs[i].key, key) == 0) {
            return i;
        }
    }
    return -1;
}

JsonType jsonGetMemberType(JsonObject *obj, const char *key)
{
    int i = jsonGetPairIndex(obj, key);

    if (i == -1) {
        return JSON_NULL;
    } else {
        return jsonGetType(obj->pairs[i].val);
    }
}

void jsonGetInt(JsonObject *parent, const char *key, int *num,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_NUMBER) {
        *num = (int) ((JsonNumber*) parent->pairs[i].val)->data;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected an int)", key);
    }
}

void jsonGetUInt(JsonObject *parent, const char *key, unsigned int *num,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_NUMBER) {
        *num = (unsigned int) ((JsonNumber*) parent->pairs[i].val)->data;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected an int)", key);
    }
}

void jsonGetBool(JsonObject *parent, const char *key, unsigned int *boolean,
        int nullIsFalse, JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_TRUE || t == JSON_FALSE || (t == JSON_NULL && nullIsFalse)) {
        *boolean = t == JSON_TRUE;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected a boolean)", key);
    }
}

void jsonGetDouble(JsonObject *parent, const char *key, double *d,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_NUMBER) {
        *d = ((JsonNumber*) parent->pairs[i].val)->data;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected a double)", key);
    }
}

void jsonGetFloat(JsonObject *parent, const char *key, float *f,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_NUMBER) {
        *f = (float) ((JsonNumber*) parent->pairs[i].val)->data;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected a double)", key);
    }
}

void jsonGetString(JsonObject *parent, const char *key, char **string,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_STRING) {
        *string = ((JsonString*) parent->pairs[i].val)->data;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected a String)", key);
    }
}

void jsonGetArray(JsonObject *parent, const char *key, JsonArray **array,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_ARRAY) {
        *array = parent->pairs[i].val;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected an Array)", key);
    }
}

void jsonGetObject(JsonObject *parent, const char *key, JsonObject **obj,
        JsonError *err)
{
    int i = jsonGetPairIndex(parent, key);
    if (i == -1) {
        JSON_ERROR(err, "Couldn't find element with key \"%s\"", key);
        return;
    }

    JsonType t = jsonGetType(parent->pairs[i].val);
    if (t == JSON_OBJECT) {
        *obj = parent->pairs[i].val;
    } else if (t == JSON_NULL) {
        JSON_ERROR(err, "Element with key \"%s\" is null", key);
    } else {
        JSON_ERROR(err, "Element with key \"%s\" is of the wrong type "
                "(expected an Object)", key);
    }
}

static void cleanupObject(JsonObject *obj);

static void cleanupArray(JsonArray *arr)
{
    for (unsigned int i = 0; i < arr->used; i++) {
        void *val = arr->vals[i];
        if (jsonGetType(val) == JSON_OBJECT) {
            cleanupObject(val);
        } else if (jsonGetType(val) == JSON_ARRAY) {
            cleanupArray(val);
        } else if (jsonGetType(val) == JSON_STRING) {
            free(((JsonString *) val)->data);
        }
        free(val);
    }
    if (arr->size != 0)
        free(arr->vals);
}

static void cleanupObject(JsonObject *obj)
{
    for (unsigned int i = 0; i < obj->used; i++) {
        JsonPair pair = obj->pairs[i];
        if (jsonGetType(pair.val) == JSON_OBJECT) {
            cleanupObject(pair.val);
        } else if (jsonGetType(pair.val) == JSON_ARRAY) {
            cleanupArray(pair.val);
        } else if (jsonGetType(pair.val) == JSON_STRING) {
            free(((JsonString *) pair.val)->data);
        }
        free(pair.key);
        free(pair.val);
    }
    if (obj->size != 0)
        free(obj->pairs);
}

void jsonCleanup(JsonObject *obj)
{
    cleanupObject(obj);
    free(obj);
}

static void writeDepth(FILE *file, int indentSize, int depth)
{
    if (depth == 0) {
        return;
    }

    fprintf(file, "%*c", indentSize * depth, ' ');
}

static void writeString(FILE *file, char *str)
{
    fprintf(file, "\"");

    for (unsigned int i = 0; i < strlen(str); i++) {
        char c = str[i];

        switch (c) {
        case '"':
            fprintf(file, "\\\"");
            break;
        case '\\':
            fprintf(file, "\\\\");
            break;
        case '\b':
            fprintf(file, "\\b");
            break;
        case '\f':
            fprintf(file, "\\f");
            break;
        case '\n':
            fprintf(file, "\\n");
            break;
        case '\r':
            fprintf(file, "\\r");
            break;
        case '\t':
            fprintf(file, "\\t");
            break;
        default:
            fprintf(file, "%c", c);
        }
    }
    fprintf(file, "\"");
}

static void writeArray(FILE *file, JsonArray *arr, int indentSize, int depth);
static void writeObject(FILE *file, JsonObject *obj, int indentSize, int depth);

static void writeValue(FILE *file, void *val, int indentSize, int depth)
{
    enum JsonType type = jsonGetType(val);

    switch (type) {
    case JSON_STRING:
        writeString(file, ((JsonString *) val)->data);
        break;
    case JSON_NUMBER:
        fprintf(file, "%g", ((JsonNumber *) val)->data);
        break;
    case JSON_OBJECT:
        writeObject(file, val, indentSize, depth);
        break;
    case JSON_ARRAY:
        writeArray(file, val, indentSize, depth);
        break;
    case JSON_TRUE:
        fprintf(file, "true");
        break;
    case JSON_FALSE:
        fprintf(file, "false");
        break;
    case JSON_NULL:
        fprintf(file, "null");
        break;
    }
}

static void writeArray(FILE *file, JsonArray *arr, int indentSize, int depth)
{
    fprintf(file, "[\n");

    for (unsigned int i = 0; i < arr->used; i++) {
        writeDepth(file, indentSize, depth + 1);

        writeValue(file, arr->vals[i], indentSize, depth + 1);

        if (i != arr->used - 1) {
            fprintf(file, ",");
        }
        fprintf(file, "\n");
    }

    writeDepth(file, indentSize, depth);
    fprintf(file, "]");
}

static void writeObject(FILE *file, JsonObject *obj, int indentSize, int depth)
{
    fprintf(file, "{\n");

    for (unsigned int i = 0; i < obj->used; i++) {
        struct JsonPair pair = obj->pairs[i];

        writeDepth(file, indentSize, depth + 1);

        fprintf(file, "\"%s\": ", pair.key);

        writeValue(file, pair.val, indentSize, depth + 1);

        if (i != obj->used - 1) {
            fprintf(file, ",");
        }

        fprintf(file, "\n");
    }

    writeDepth(file, indentSize, depth);
    fprintf(file, "}");
}

void jsonWriteObject(FILE *file, JsonObject *obj, int indentSize)
{
    writeObject(file, obj, indentSize, 0);

    fflush(file);
}
